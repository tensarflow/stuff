hoursPerDay = 10;
coeff24hMA = ones(1, hoursPerDay)/hoursPerDay;

a = filter(coeff24hMA, 1, robotdatatest.actual_robot_current);

stackedplot(a)
T = table(a)
writetable(T)