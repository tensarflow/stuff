# To save all streaming data of the robot to a csv file:

1. Download the folder rtde_client_3.5

2. Connect your UR with an crossover ethernet cable to your PC and set your IPv4 Settings to Link-Local Only. (May require reboot, close other connections such as wlan)

3. Set the UR-IP to the same as your pc's. Only last number is defferent.

4. Open terminal in directory `.../rtde_client_3.5/examples` and execute `python record.py --host 169.254.156.XX` , where `XX` should be the numbers of your UR's IP.
