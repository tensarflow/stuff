from robolink import *
from robodk import * 

RDK = Robolink()

RDK.AddFile('C:/dev/stuff/UR3.robot')

robot = RDK.Item('UR3')

if not robot.Valid():
    raise Exception('No robot selected or available')


success = robot.Connect('172.17.34.32')
success = robot.ConnectSafe()

if success:
    # Stop if the connection did not succeed
    print("Failed to connect!")


robot.MoveJ([0,0,0,0,10,-200])

print('Done')
