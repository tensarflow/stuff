import socket
import math

UDP_IP = "10.254.184.179"
UDP_PORT = 80
MESSAGE = 0.0

print "UDP target IP:", UDP_IP
print "UDP target port:", UDP_PORT
print "message:", MESSAGE

while True:

	sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	sock.sendto(str(math.sin(MESSAGE)), (UDP_IP, UDP_PORT))
	
	print "message sent: ", math.sin(MESSAGE)
	MESSAGE = MESSAGE + 0.01