import socket
import struct

address = ("192.168.43.240", 1234)

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(address)
print ('Listening...')
s.listen(1000)


client, addr = s.accept()
print ('got connected from', addr)

buf = ''
while len(buf)<4:
	buf += client.recv(4-len(buf))
size = struct.unpack('!i', buf)
print ("receiving %s bytes" % size)

with open('tst.png', 'wb') as img:
	while True:
		data = client.recv(1024)
		if not data:
			break
		img.write(data)
print ('received, yay!')

client.close()