# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'C:\dev\stuff\SensorSim\SensorSim.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets
import threading
from threading import Thread
import socket
import csv
import zmq
from pathlib import Path
from time import sleep

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(913, 899)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton.setGeometry(QtCore.QRect(360, 20, 75, 191))
        self.pushButton.setObjectName("pushButton")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(20, 20, 331, 191))
        self.label.setText("")
        self.label.setPixmap(QtGui.QPixmap("data/ref_1.png"))
        self.label.setScaledContents(True)
        self.label.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignVCenter)
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(20, 230, 331, 191))
        self.label_2.setText("")
        self.label_2.setPixmap(QtGui.QPixmap("data/ref_2.png"))
        self.label_2.setScaledContents(True)
        self.label_2.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignVCenter)
        self.label_2.setObjectName("label_2")
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setGeometry(QtCore.QRect(20, 440, 331, 191))
        self.label_3.setText("")
        self.label_3.setPixmap(QtGui.QPixmap("data/ref_3.png"))
        self.label_3.setScaledContents(True)
        self.label_3.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignVCenter)
        self.label_3.setObjectName("label_3")
        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        self.label_4.setGeometry(QtCore.QRect(20, 650, 331, 191))
        self.label_4.setText("")
        self.label_4.setPixmap(QtGui.QPixmap("data/ref_4.png"))
        self.label_4.setScaledContents(True)
        self.label_4.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignVCenter)
        self.label_4.setObjectName("label_4")
        self.pushButton_2 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_2.setGeometry(QtCore.QRect(360, 230, 75, 191))
        self.pushButton_2.setObjectName("pushButton_2")
        self.pushButton_3 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_3.setGeometry(QtCore.QRect(360, 440, 75, 191))
        self.pushButton_3.setObjectName("pushButton_3")
        self.pushButton_4 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_4.setGeometry(QtCore.QRect(360, 650, 75, 191))
        self.pushButton_4.setObjectName("pushButton_4")
        self.pushButton_5 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_5.setGeometry(QtCore.QRect(440, 20, 75, 191))
        self.pushButton_5.setObjectName("pushButton_5")
        self.pushButton_6 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_6.setGeometry(QtCore.QRect(440, 230, 75, 191))
        self.pushButton_6.setObjectName("pushButton_6")
        self.pushButton_7 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_7.setGeometry(QtCore.QRect(440, 440, 75, 191))
        self.pushButton_7.setObjectName("pushButton_7")
        self.pushButton_8 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_8.setGeometry(QtCore.QRect(440, 650, 75, 191))
        self.pushButton_8.setObjectName("pushButton_8")
        self.radioButton = QtWidgets.QRadioButton(self.centralwidget)
        self.radioButton.setGeometry(QtCore.QRect(540, 100, 101, 31))
        self.radioButton.setObjectName("radioButton")
        self.radioButton.setChecked(True)
        self.radioButton_2 = QtWidgets.QRadioButton(self.centralwidget)
        self.radioButton_2.setGeometry(QtCore.QRect(540, 310, 101, 31))
        self.radioButton_2.setObjectName("radioButton_2")
        self.radioButton_2.setChecked(True)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 913, 21))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

        #Assigning functions to buttons
        self.pushButton.clicked.connect(self.sendData_ref_1)
        self.pushButton_5.clicked.connect(self.stopSending_ref_1)
        self.pushButton_2.clicked.connect(self.sendData_ref_2)
        self.pushButton_6.clicked.connect(self.stopSending_ref_2)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.pushButton.setText(_translate("MainWindow", "Send Data"))
        self.pushButton_2.setText(_translate("MainWindow", "Send Data"))
        self.pushButton_3.setText(_translate("MainWindow", "Send Data"))
        self.pushButton_4.setText(_translate("MainWindow", "Send Data"))
        self.pushButton_5.setText(_translate("MainWindow", "Stop"))
        self.pushButton_6.setText(_translate("MainWindow", "Stop"))
        self.pushButton_7.setText(_translate("MainWindow", "Stop"))
        self.pushButton_8.setText(_translate("MainWindow", "Stop"))
        self.radioButton.setText(_translate("MainWindow", "Detect Ref_1"))
        self.radioButton_2.setText(_translate("MainWindow", "Detect Ref_2"))

    def sendData_ref_1(self):
        self.ref_1_stop = threading.Event()
        self.myThreadRef_1 = MyThread(self.ref_1_stop, "data/ref_1.csv")
        self.myThreadRef_1.setName('Ref_1')
        try:
            self.myThreadRef_1.start()
        except Exception as e:
            print(e)

    def stopSending_ref_1(self):
        self.myThreadRef_1.stopEvent.set()

    def sendData_ref_2(self):
        self.ref_2_stop = threading.Event()
        self.myThreadRef_2 = MyThread(self.ref_2_stop, "data/gooddata.csv")
        self.myThreadRef_2.setName('Ref_2')
        try:
            self.myThreadRef_2.start()
        except Exception as e:
            print(e)

    def stopSending_ref_2(self):
        self.myThreadRef_2.stopEvent.set()

class MyThread(Thread):

    def __init__(self, stopEvent, pathToCSVFile):
        ''' Constructor. '''
        Thread.__init__(self)
        self.stopEvent = stopEvent
        self.pathToCSVFile = pathToCSVFile
        self.UDP_PORT = 80

    def run(self):
        print ("Thread started")
        UDP_IP = "127.0.0.1"
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        d = []
        with open(self.pathToCSVFile) as f:
            reader = csv.reader(f, delimiter=';')
            for row in reader:
                if row:
                    d.append(row)

        while not self.stopEvent.is_set():
            for i in range(100):
                sock.sendto("0.0".encode(), (UDP_IP, self.UDP_PORT))
                print ("message sent from "+ self.name +": 0.0")
                sleep(0.02)

            for i in range(len(d)):
                for j in range(len(d[i])):
                    sock.sendto(str(d[i][j]).encode(), (UDP_IP, self.UDP_PORT))
                    print ("message sent from "+ self.name +": "+ str(d[i][j]))
                    sleep(0.02)

            for i in range(100):
                sock.sendto("0.0".encode(), (UDP_IP, self.UDP_PORT))
                print ("message sent from "+ self.name +": 0.0")
                sleep(0.02)

        print ("Sending to port "+ str(self.UDP_PORT) +" from "+ self.name +" terminated...")

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
