#!/usr/bin/env python

import socket


TCP_IP = '10.140.124.102'
TCP_PORT = 13085
BUFFER_SIZE = 1024

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((TCP_IP, TCP_PORT))

data = s.recv(BUFFER_SIZE)

s.close()

print 'Received', repr(data)