from socket import *
import time

HOST = "172.17.34.35"
PORT = 80

address = (HOST, PORT) #Defind who you are talking to (must match arduino IP and port)
client_socket = socket(AF_INET, SOCK_DGRAM) #Set Up the Socket
client_socket.bind((HOST, PORT)) 
client_socket.settimeout(5) #only wait 1 second for a response

while(1): #Main Loop
	rec_data, addr = client_socket.recvfrom(100)  #Read response from arduino
	print rec_data #Print the response from Arduino
	time.sleep(10/1000000) # sleep 10 microseconds